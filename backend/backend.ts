import crypto from 'crypto';
import path from 'path';
import { promisify } from 'util';
import dotenv from 'dotenv';
import express from 'express';
import { Pool, PoolClient } from 'pg';
import * as R from 'ramda';
const notEmptyStr : (s: string) => boolean = R.compose(R.not, R.isEmpty);

dotenv.config();

const TOKEN_CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
const validateToken = R.test(new RegExp('^[' + TOKEN_CHARS + ']*$'));

const randomBytesAysnc = promisify(crypto.randomBytes);

function generateShortToken() {
    return randomBytesAysnc(5).then((buf) => Array.from(buf)
        .map(randomByte => randomByte / 256) // map byte (8 bit unsigned int, max value 2^8-1) to floating point between 0 (inclusive) and 1 (exclusive)
        .map(randomFrac => Math.floor(randomFrac * TOKEN_CHARS.length))
        .map(randomIndex => TOKEN_CHARS[randomIndex])
        .join(''));
}

function isTokenAlreadyTaken(dbClient: PoolClient, token: string) {
    return dbClient.query('SELECT EXISTS(SELECT 1 FROM urlshortener WHERE token=$1)', [token])
        .then(({ rows }) => { return rows[0].exists; });
}
function getUniqueToken(dbClient: PoolClient) {
    function attemptToGenerateUniqueToken(retriesLeft: number): Promise<string> {
        return generateShortToken()
            .then((newToken) => {
                return isTokenAlreadyTaken(dbClient, newToken)
                    .then(isTaken => !isTaken ?
                        Promise.resolve(newToken) :
                        retriesLeft > 0 ?
                            attemptToGenerateUniqueToken(retriesLeft - 1) :
                            Promise.reject("Couldn't generate unique token"));
            });
    }
    return attemptToGenerateUniqueToken(4);
}

function getShortToken(dbPool: Pool, longUrl: string, wantedToken: string) {
    return dbPool.connect().then(dbClient => {
        return Promise.resolve().then(() => {
            if (notEmptyStr(wantedToken)) {
                if (!validateToken(wantedToken))
                    return Promise.reject(`Invalid token! Valid characters: ${TOKEN_CHARS.split('').join(',')}`);

                return isTokenAlreadyTaken(dbClient, wantedToken)
                    .then(isTaken => isTaken ?
                        Promise.reject(`'${wantedToken}' is already taken! :(`) :
                        Promise.resolve(wantedToken));
            }

            return getUniqueToken(dbClient);
        })
        .then(tokenToInsert => dbClient.query('BEGIN')
            .then(() => dbClient.query('INSERT INTO urlshortener(token, url) VALUES($1, $2)', [tokenToInsert, longUrl]))
            .catch(() => dbClient.query('ROLLBACK').then(() => Promise.reject("Couldn't insert token to database")))
            .then(() => dbClient.query('COMMIT'))
            .then(() => tokenToInsert)
        )
        .finally(() => dbClient.release());
    });
}
function getLongUrl(dbPool: Pool, token: string): Promise<string> {
    return dbPool.query('SELECT url FROM urlshortener WHERE token=$1', [token])
        .then<string>(({ rows, rowCount }) => {
            if (rowCount === 0) {
                return Promise.reject("Short URL is not registered!");
            }
            return rows[0].url;
        });
}

const server = express();

server.use(express.static('build'));

const dbPool = new Pool({
    user: process.env.PGUSER,
    database: process.env.PGDATABASE
});

server.get('/shorten', (req, res) => {
    const url = req.query.url;
    const wantedToken = req.query.wantedToken || '';
    getShortToken(dbPool, url, wantedToken)
        .then(token => res.json({error: '', token, url}))
        .catch(error => res.json({error, token: '', url}));
});

server.get(new RegExp('^\/[' + TOKEN_CHARS + ']+$'), (req, res) => {
    getLongUrl(dbPool, path.basename(req.path))
        .then(longUrl => res.redirect(301, longUrl))
        .catch(error => res.status(404).end(error));
});

const PORT: number = <any> process.env.PORT;
server.listen(PORT, () => console.log(`Listening on port ${PORT}`));
