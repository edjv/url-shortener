import React from 'react';
import './App.css';
import * as R from 'ramda';
const notEmptyStr : (s: string) => boolean = R.compose(R.not, R.isEmpty);

interface Result {
  success: boolean;
  feedback: string;
  shortUrl: string;
  redirectTargetUrl: string;
}

const App: React.FC = () => {
  const [inputUrl, setInputUrl] = React.useState('');
  const [wantedToken, setWantedToken] = React.useState('');
  const [result, setResult] = React.useState({
    success: false,
    feedback: '',
    shortUrl: '',
    redirectTargetUrl: ''
  });

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!validateUrl(inputUrl)) {
      setResult(() => R.mergeRight(result, {
        success: false,
        feedback: 'Please input a valid URL (hint: start with a protocol, e.g., `http://` or `https://`)'
      }));
      return;
    }

    const queryUrl = new URL('shorten', window.location.href);
    queryUrl.searchParams.set('url', inputUrl);
    if (notEmptyStr(wantedToken))
      queryUrl.searchParams.set('wantedToken', wantedToken);

    fetch(queryUrl.toString())
      .then<{error: string, token: string, url: string}>(resp => resp.json())
      .then(({ error, token, url}) => notEmptyStr(error) ? Promise.reject(error) : Promise.resolve({token, url}))
      .then(({token, url}) => ({
        from: R.toString(new URL(token, window.location.href)),
        to: url
      }))
      .then(({from, to}) => {
        setResult(() => R.mergeRight(result, {
          success: true,
          feedback: 'Short URL created!',
          shortUrl: from,
          redirectTargetUrl: to
        }));
        setInputUrl('');
        setWantedToken('');
      })
      .catch(error => setResult(() => R.mergeRight(result, { success: false, feedback: error })));
  };

  return (
    <div className="App">
      <br />
      <form onSubmit={handleSubmit}>
        <label>
          Long URL:
          <input className="url" type="text" value={inputUrl} onChange={toInputChangeHandler(setInputUrl)}></input>
        </label>
        <input type="submit" value="Shorten it!"/>
        <label>
          Preferred token:
          <input type="text" value={wantedToken} onChange={toInputChangeHandler(setWantedToken)}></input>
        </label>
      </form>
      <Result {...result} />
    </div>
  );
};

const Result: React.FC<Result> = (props) => {
  const feedbackStyle = {color: props.success ? 'green' : 'red'};
  return (
    <div>
      {notEmptyStr(props.feedback) &&
        <p style={feedbackStyle}>{props.feedback}</p>}
      {props.success &&
        <p><a href={props.shortUrl} target="_blank">{props.shortUrl}</a> -> <small>{props.redirectTargetUrl}</small></p>}
    </div>
  );
};

function validateUrl(url: string) {
  try {
    new URL(url);
    return true;
  } catch(_) {
    return false;
  }
}

function toInputChangeHandler(setState: React.Dispatch<React.SetStateAction<string>>) {
  return (event: React.ChangeEvent<HTMLInputElement>) => setState(R.trim(event.target.value));
}

export default App;
